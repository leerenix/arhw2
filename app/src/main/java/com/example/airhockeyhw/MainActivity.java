package com.example.airhockeyhw;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.graphics.Color;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private GLSurfaceView glSurfaceView;
    private boolean rendererSet = false;
    private static TextView scoreTop;
    private static TextView scoreDown;
    private static int blueScore = 0;
    private static int redScore = 0;
    public static MainActivity mn;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mn = MainActivity.this;
        glSurfaceView = new GLSurfaceView(this);

        ActivityManager activityManager = 
            (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        ConfigurationInfo configurationInfo = activityManager
            .getDeviceConfigurationInfo();

        final boolean supportsEs2 =
            configurationInfo.reqGlEsVersion >= 0x20000
                || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1
                 && (Build.FINGERPRINT.startsWith("generic")
                  || Build.FINGERPRINT.startsWith("unknown")
                  || Build.MODEL.contains("google_sdk")
                  || Build.MODEL.contains("Emulator")
                  || Build.MODEL.contains("Android SDK built for x86")));

        final AirHockeyRenderer airHockeyRenderer = new AirHockeyRenderer(this);
        
        if (supportsEs2) {

            glSurfaceView.setEGLContextClientVersion(2);

            glSurfaceView.setRenderer(airHockeyRenderer);
            rendererSet = true;
        } else {

            Toast.makeText(this, "This device does not support OpenGL ES 2.0.",
                Toast.LENGTH_LONG).show();
            return;
        }

        
glSurfaceView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event != null) {

                    final float normalizedX = 
                        (event.getX() / (float) v.getWidth()) * 2 - 1;
                    final float normalizedY = 
                        -((event.getY() / (float) v.getHeight()) * 2 - 1);
                    
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        glSurfaceView.queueEvent(new Runnable() {
                            @Override
                            public void run() {
                                airHockeyRenderer.handleTouchPress(
                                    normalizedX, normalizedY);
                            }
                        });
                    } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
                        glSurfaceView.queueEvent(new Runnable() {
                            @Override
                            public void run() {
                                airHockeyRenderer.handleTouchDrag(
                                    normalizedX, normalizedY);
                            }
                        });
                    }                    

                    return true;                    
                } else {
                    return false;
                }
            }
        });


scoreDown = new TextView(this);
        scoreDown.setText("0");
        scoreDown.setTextColor(Color.WHITE);
        scoreDown.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
        FrameLayout.LayoutParams paramsDown = new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        paramsDown.gravity = (Gravity.CENTER | Gravity.BOTTOM);
        addContentView(scoreDown, paramsDown);

        scoreTop = new TextView(this);
        scoreTop.setText("0");
        scoreTop.setTextColor(Color.WHITE);
        scoreTop.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
        FrameLayout.LayoutParams paramsTop = new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        paramsTop.gravity = (Gravity.CENTER | Gravity.TOP);
        scoreTop.setRotation(180);
        addContentView(scoreTop, paramsTop);
    }

        setContentView(glSurfaceView);
        }

    @Override
    protected void onPause() {
        super.onPause();
        
        if (rendererSet) {
            glSurfaceView.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        
        if (rendererSet) {
            glSurfaceView.onResume();
        }
    }


    public static void increaseBlueScore(){
        blueScore++;
        mn.runOnUiThread(new Runnable() {
            public void run() {
                scoreDown.setText(Integer.toString(blueScore));
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AirHockeyRenderer.redHit = true;
                    }
                }, 1000);
            }
        });

    }
    public static void increaseRedScore(){
        redScore++;
        mn.runOnUiThread(new Runnable() {
            public void run() {
                scoreTop.setText(Integer.toString(redScore));
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AirHockeyRenderer.blueHit = true;
                    }
                }, 1000);
            }
        });
 	}
}