package com.example.airhockeyhw;

import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glClearColor;
import static android.opengl.GLES20.glViewport;
import static android.opengl.Matrix.invertM;
import static android.opengl.Matrix.multiplyMM;
import static android.opengl.Matrix.multiplyMV;
import static android.opengl.Matrix.rotateM;
import static android.opengl.Matrix.setIdentityM;
import static android.opengl.Matrix.setLookAtM;
import static android.opengl.Matrix.translateM;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLSurfaceView.Renderer;

import android.util.Log;
import com.example.airhockeyhw.objects.Mallet;
import com.example.airhockeyhw.objects.Puck;
import com.example.airhockeyhw.objects.Table;
import com.example.airhockeyhw.programs.ColorShaderProgram;
import com.example.airhockeyhw.programs.TextureShaderProgram;
import com.example.airhockeyhw.util.Geometry;
import com.example.airhockeyhw.util.Geometry.Plane;
import com.example.airhockeyhw.util.Geometry.Point;
import com.example.airhockeyhw.util.Geometry.Ray;
import com.example.airhockeyhw.util.Geometry.Sphere;
import com.example.airhockeyhw.util.Geometry.Vector;
import com.example.airhockeyhw.util.MatrixHelper;
import com.example.airhockeyhw.util.TextureHelper;



public class AirHockeyRenderer implements Renderer {    
    private final Context context;

    private final float[] projectionMatrix = new float[16];
    private final float[] modelMatrix = new float[16];
    private final float[] viewMatrix = new float[16];
    private final float[] viewProjectionMatrix = new float[16];
    private final float[] invertedViewProjectionMatrix = new float[16];
    private final float[] modelViewProjectionMatrix = new float[16];                       

    private Table table;
    private Mallet mallet;
    private Puck puck;           
    static boolean redHit = true;
    static boolean blueHit = true;
    private TextureShaderProgram textureProgram;
    private ColorShaderProgram colorProgram;

    private int texture;
    private boolean malletPressed = false;
    private boolean blueMalletPressed = false;
    private Point blueMalletPosition;
    private Point redMalletPosition;

    private final float leftBound = -0.5f;
    private final float rightBound = 0.5f;
    private final float farBound = -0.8f;
    private final float nearBound = 0.8f;


    private Point puckPosition;
    private Vector puckVector;

    AirHockeyRenderer(Context context) {
        this.context = context;
    }

    void handleTouchPress(float normalizedX, float normalizedY) {
        
        Ray ray = convertNormalized2DPointToRay(normalizedX, normalizedY);

        Sphere blueMalletBoundingSphere = new Sphere(new Point(
                blueMalletPosition.x, 
                blueMalletPosition.y, 
                blueMalletPosition.z),
            mallet.height / 2f);

        malletPressed = Geometry.intersects(blueMalletBoundingSphere, ray);
        if (malletPressed==false) {
            Sphere redMalletBoundingSphere = new Sphere(new Point(
                    redMalletPosition.x,
                    redMalletPosition.y,
                    redMalletPosition.z),
                    mallet.height / 2f);

            malletPressed = Geometry.intersects(redMalletBoundingSphere, ray);
            blueMalletPressed = false;
        }else {
            blueMalletPressed = true;
        }
        Log.w("MalletPressed", "Blue mallet pressed: "+blueMalletPressed);
    }
    
    private Ray convertNormalized2DPointToRay(
        float normalizedX, float normalizedY) {
        final float[] nearPointNdc = {normalizedX, normalizedY, -1, 1};
        final float[] farPointNdc =  {normalizedX, normalizedY,  1, 1};
        
        final float[] nearPointWorld = new float[4];
        final float[] farPointWorld = new float[4];

        multiplyMV(
            nearPointWorld, 0, invertedViewProjectionMatrix, 0, nearPointNdc, 0);
        multiplyMV(
            farPointWorld, 0, invertedViewProjectionMatrix, 0, farPointNdc, 0);

        divideByW(nearPointWorld);
        divideByW(farPointWorld);

        Point nearPointRay = 
            new Point(nearPointWorld[0], nearPointWorld[1], nearPointWorld[2]);
			
        Point farPointRay = 
            new Point(farPointWorld[0], farPointWorld[1], farPointWorld[2]);

        return new Ray(nearPointRay, 
                       Geometry.vectorBetween(nearPointRay, farPointRay));
    }        

    private void divideByW(float[] vector) {
        vector[0] /= vector[3];
        vector[1] /= vector[3];
        vector[2] /= vector[3];
    }

    
    void handleTouchDrag(float normalizedX, float normalizedY) {
        
        if (malletPressed) {
            Ray ray = convertNormalized2DPointToRay(normalizedX, normalizedY);

            Plane plane = new Plane(new Point(0, 0, 0), new Vector(0, 1, 0));

            Point touchedPoint = Geometry.intersectionPoint(ray, plane);

            if(blueMalletPressed){
                Point previousBlueMalletPosition = blueMalletPosition;

                blueMalletPosition = new Point(
                        clamp(touchedPoint.x,
                                leftBound + mallet.radius,
                                rightBound - mallet.radius),
                        mallet.height / 2f,
                        clamp(touchedPoint.z,
                                0f + mallet.radius,
                                nearBound - mallet.radius));

                float distance =
                        Geometry.vectorBetween(blueMalletPosition, puckPosition).length();

                if (distance < (puck.radius + mallet.radius)) {

                    puckVector = Geometry.vectorBetween(
                            previousBlueMalletPosition, blueMalletPosition);
                }
            }else{

                Point previousRedMalletPosition = redMalletPosition;

                redMalletPosition = new Point(
                        clamp(touchedPoint.x,
                                leftBound + mallet.radius,
                                rightBound - mallet.radius),
                        mallet.height / 2f,
                        clamp(touchedPoint.z,

                                farBound + mallet.radius, 0f - mallet.radius));

                float distance =
                        Geometry.vectorBetween(redMalletPosition, puckPosition).length();

                if (distance < (puck.radius + mallet.radius)) {

                    puckVector = Geometry.vectorBetween(
                            previousRedMalletPosition, redMalletPosition);
                }

            }
                        

        }
    }
    
    private float clamp(float value, float min, float max) {
        return Math.min(max, Math.max(value, min));
    }

    @Override
    public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);        

        table = new Table();
        mallet = new Mallet(0.08f, 0.15f, 32);
        puck = new Puck(0.06f, 0.02f, 32);
        
        blueMalletPosition = new Point(0f, mallet.height / 2f, 0.4f);
        redMalletPosition = new Point(0f, mallet.height / 2f, -0.4f);

        puckPosition = new Point(0f, puck.height / 2f, 0f);
        puckVector = new Vector(0f, 0f, 0f);

        textureProgram = new TextureShaderProgram(context);
        colorProgram = new ColorShaderProgram(context);

        texture = TextureHelper.loadTexture(context, R.drawable.air_hockey_surface);


    }

    @Override
    public void onSurfaceChanged(GL10 glUnused, int width, int height) {                

        glViewport(0, 0, width, height);        

        MatrixHelper.perspectiveM(projectionMatrix, 45, (float) width
            / (float) height, 1f, 10f);

        setLookAtM(viewMatrix, 0, 0f, 1.2f, 2.2f, 0f, 0f, 0f, 0f, 1f, 0f);                
    }

    @Override
    public void onDrawFrame(GL10 glUnused) {

        glClear(GL_COLOR_BUFFER_BIT);

        puckPosition = puckPosition.translate(puckVector);

        if (puckPosition.x < leftBound + puck.radius
         || puckPosition.x > rightBound - puck.radius) {
            puckVector = new Vector(-puckVector.x, puckVector.y, puckVector.z);
            puckVector = puckVector.scale(0.9f);
        }        
        if (puckPosition.z < farBound + puck.radius
         || puckPosition.z > nearBound - puck.radius) {
            puckVector = new Vector(puckVector.x, puckVector.y, -puckVector.z);
            puckVector = puckVector.scale(0.9f);
        }        

        puckPosition = new Point(
            clamp(puckPosition.x, leftBound + puck.radius, rightBound - puck.radius),
            puckPosition.y,
            clamp(puckPosition.z, farBound + puck.radius, nearBound - puck.radius)
        );

        puckVector = puckVector.scale(0.99f);

        multiplyMM(viewProjectionMatrix, 0, projectionMatrix, 0,
            viewMatrix, 0);
        invertM(invertedViewProjectionMatrix, 0, viewProjectionMatrix, 0);

        positionTableInScene();
        textureProgram.useProgram();
        textureProgram.setUniforms(modelViewProjectionMatrix, texture);
        table.bindData(textureProgram);
        table.draw();

        positionObjectInScene(redMalletPosition.x, redMalletPosition.y, redMalletPosition.z);
        colorProgram.useProgram();
        colorProgram.setUniforms(modelViewProjectionMatrix, 1f, 0f, 0f);
        mallet.bindData(colorProgram);
        mallet.draw();                        

        positionObjectInScene(blueMalletPosition.x, blueMalletPosition.y,
            blueMalletPosition.z);
        colorProgram.setUniforms(modelViewProjectionMatrix, 0f, 0f, 1f);

        mallet.draw();

        positionObjectInScene(puckPosition.x, puckPosition.y, puckPosition.z);
        colorProgram.setUniforms(modelViewProjectionMatrix, 0.8f, 0.8f, 1f);
        puck.bindData(colorProgram);
        puck.draw();

        Log.w("POS", Float.toString(puckPosition.x));

 if(redHit && puckPosition.z<-0.7 && puckPosition.x > -0.1 && puckPosition.x < 0.1){
            redHit = false;


            MainActivity.increaseBlueScore();


        }else if(blueHit && puckPosition.z>0.7 && puckPosition.x > -0.1 && puckPosition.x < 0.1){
           blueHit = false;

            MainActivity.increaseRedScore();
        }
      
    }

    private void positionTableInScene() {

        setIdentityM(modelMatrix, 0);
        rotateM(modelMatrix, 0, -90f, 1f, 0f, 0f);
        multiplyMM(modelViewProjectionMatrix, 0, viewProjectionMatrix,
            0, modelMatrix, 0);
    }

    private void positionObjectInScene(float x, float y, float z) {
        setIdentityM(modelMatrix, 0);
        translateM(modelMatrix, 0, x, y, z);
        multiplyMM(modelViewProjectionMatrix, 0, viewProjectionMatrix,
            0, modelMatrix, 0);
    }
}