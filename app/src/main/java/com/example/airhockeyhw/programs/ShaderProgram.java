
package com.example.airhockeyhw.programs;

import static android.opengl.GLES20.glUseProgram;
import android.content.Context;

import com.example.airhockeyhw.util.ShaderHelper;
import com.example.airhockeyhw.util.TextResourceReader;

abstract class ShaderProgram {

    static final String U_MATRIX = "u_Matrix";
    static final String U_COLOR = "u_Color";
    static final String U_TEXTURE_UNIT = "u_TextureUnit";


    static final String A_POSITION = "a_Position";
    protected static final String A_COLOR = "a_Color";    
    static final String A_TEXTURE_COORDINATES = "a_TextureCoordinates";

    final int program;

    ShaderProgram(Context context, int vertexShaderResourceId,
                  int fragmentShaderResourceId) {

        program = ShaderHelper.buildProgram(
            TextResourceReader
                .readTextFileFromResource(context, vertexShaderResourceId),
            TextResourceReader
                .readTextFileFromResource(context, fragmentShaderResourceId));
    }        

    public void useProgram() {

        glUseProgram(program);
    }
}
